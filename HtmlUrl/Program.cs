﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace HtmlUrl
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Введите URL-адрес: ");

            Console.ForegroundColor = ConsoleColor.DarkGray;
            string urlAdres = Convert.ToString(Console.ReadLine());
            Console.ResetColor();

            string url = urlAdres, result = GetUrl(url);

            DumpHrefs(result);

            Console.WriteLine("");

            Console.ReadLine();
        }

        public static string GetUrl(string address)
        {
            WebClient client = new WebClient();

            client.Credentials = CredentialCache.DefaultNetworkCredentials;

            return client.DownloadString(address);
        }

        private static void DumpHrefs(string html)
        {
            Match match;

            string pattern = @"['""](https?.+?)['""]";

            try
            {
                match = Regex.Match(html, pattern);
                List<string> HREFS = new List<string>();

                Console.WriteLine();

                while (match.Success)
                {
                    Console.WriteLine($"Найденный URL-адрес:  {match.Groups[1]}");
                    HREFS.Add(match.Groups[1].Value);
                    match = match.NextMatch();
                }

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"\nКоличество найденных URL-адресов: {HREFS.Count} шт.");
                Console.ResetColor();

            }
            catch (RegexMatchTimeoutException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nОшибка в работе приложения.");
                Console.ResetColor();
            }
        }
    }
}
